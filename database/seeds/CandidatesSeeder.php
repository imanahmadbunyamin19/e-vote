<?php

use Illuminate\Database\Seeder;
use App\Kandidat;

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kandidat::create([
            'nama_ketua'	=> 'H. IMAN AHMAD BUNYAMIN',
            'nama_wakil'	=> 'Hj. HERMA SITI ROSMAWANTI',
            'visi'	=> 'memajukan yang harus maju',
            'misi'	=> 'memundurkan yang harus mundur',
            'foto'	=> 'xxx.jpg'
        ]);
    }
}
