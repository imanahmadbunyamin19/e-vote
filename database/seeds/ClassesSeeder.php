<?php

use App\Kelas;
use Illuminate\Database\Seeder;
class ClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
            
    for ($i=1; $i < 5; $i++) { 
        Kelas::create([
            'nama_kelas' => 'IX - '. $i
        ]);
    }
          
    }
        
}
