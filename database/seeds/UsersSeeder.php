<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email'	=> 'admin@gmail.com',
            'name'	=> 'admin',
            'password'	=> bcrypt('admin1234'),
            'role'	=> 'admin'
        ]);
    }
}
