@section('judul')
Data Admin
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div>
        <form action="/admin" method="POST">
            @csrf
            <div class="input-group mb-3">
                <input type="email" name="email" class="form-control" placeholder="Email" value="{{old('email')}}">
                <div class="input-group-append">
                  <div class="input-group-text">
                      <span class="fas fa fa-university"></span>
                  </div>
                </div>
              </div>
              @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
              @enderror
      
              <div class="input-group mb-3">
                <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" value="{{old('name')}}">
                <div class="input-group-append">
                  <div class="input-group-text">
                      <span class="fas fa-user"></span>
                  </div>
                </div>
              </div>
              @error('name')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
              @enderror
      
              <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="Password" @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              @error('password')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
               @enderror
              <div class="input-group mb-3">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Retype Password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              @error('password_confirmation')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
              @enderror
              <div class="row">
                <!-- /.col -->
                <div class="col-12">
                  <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Register') }}
                  </button>
                </div>
                <!-- /.col -->
              </div>
        </form>
</div>
@endsection
