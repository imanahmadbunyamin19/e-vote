@section('judul')
Edit Data Admin
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div class="col-6">
    <form action="/admin/{{$admin->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="input-group mb-3">
            <input type="email" name="email" class="form-control" placeholder="Email" value="{{$admin->email}}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa fa-university"></span>
                </div>
            </div>
            </div>
            @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
    
            <div class="input-group mb-3">
            <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" value="{{$admin->name}}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-user"></span>
                </div>
            </div>
            </div>
            @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
    
            <div class="input-group mb-3">
            <input type="password" name="password" class="form-control" placeholder="Hanya Isi Password Jika Akan Dirubah" >
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa fa-key"></span>
                </div>
            </div>
            </div>
    
            <div class="row">
            <!-- /.col -->
            <div class="col-12">
                <button type="submit" class="btn btn-primary" >Simpan</button>
            </div>
            <!-- /.col -->
            </div>
    </form>
</div>
@endsection
