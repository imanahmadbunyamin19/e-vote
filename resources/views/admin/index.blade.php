@section('judul')
{{-- TEMPAT MEMBUAT JUDUL HALAMAN --}}
Data Admin
@endsection

@extends('template.template')

@push('script')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN JAVASCRIPT LIBRARY/CUSTOM --}}
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN CSS LIBRARY/CUSTOM --}}
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush 

@section('content')
{{-- BUAT KONTEN ANDA DIAREA SINI --}}

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Admin</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <a href="/admin/create" class="btn btn-primary mb-3">Tambah admin</a>
    <table id="example1" class="table table-striped">
            <thead >
              <tr>
                <th width="1%">No</th>
                <th>Email</th>
                <th>Nama</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($admin as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->email}}</td>
                        <td>{{$value->name}}</td>
                        <td display: inline>
                            <form action="/admin/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                {{-- <a href="/admin/{{$value->id}}" class="btn btn-info btn-sm">Show</a> --}}
                                <a href="/admin/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">Tidak ada data ditemukan</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
  </div>
</div>

@endsection
