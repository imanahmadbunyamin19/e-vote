@section('judul')
{{-- TEMPAT MEMBUAT JUDUL HALAMAN --}}
Dashboard
@endsection

@extends('template.template')

@push('script')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN JAVASCRIPT LIBRARY/CUSTOM --}}
@endpush

@push('style')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN CSS LIBRARY/CUSTOM --}}

@endpush

@section('content')
{{-- BUAT KONTEN ANDA DIAREA SINI --}}

<div class="row">
  <div class="col-lg-4 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ $jmlkelas }}</h3>
        
        <p>Jumlah Kelas</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      @auth   
      <a href="/kelas" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      @endauth
    </div>
  </div>
  <div class="col-lg-4 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>{{ $jmlsiswa }}</h3>

        <p>Jumlah Hak Pilih (Siswa)</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      @auth
        <a href="/siswa" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      @endauth
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-4 col-6">
    <!-- small box -->
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{ $jumlahvotemasuk }}</h3>

        <p>Jumlah Suara Masuk</p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      @auth
        <a href="/votes" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      @endauth
    </div>
  </div>
</div>

<div class="row">
  @for ($i = 0; $i < $jmlkandidat; $i++)
  <div class="col-md-4">
    <!-- Widget: user widget style 1 -->
    <div class="card card-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-primary ">
        <h1 class="widget-user"  style="background-color:yellow;">
            <strong style="color:black" class=""> Calon Nomor {{ $i+1 }} </strong>
        </h1>
        <h3 class="widget-user-username" style="color:black">
            <strong> {{ $kandidat[$i]->nama_ketua }}  </strong>
        </h3>

        <h3 class="widget-user-username" style="color:black">
            <strong> {{ $kandidat[$i]->nama_wakil }} </strong>
        </h3>
      </div>
      
      <div >
        <img class="img-fluid img-thumbnail mt-2" src="{{ asset('fotoKandidat/' . $kandidat[$i]->foto) }}" alt="Foto Calon">
      </div>

      <div>
        <div class="row">
            <div class="col-12 text-center mt-2">
                @method('POST')
                <div class="col-12 text-center">
                    <h1 style="background-color:yellow"> <strong> {{ $jumlahvotekandidat->where('candidate_id', '=', $kandidat[$i]->id)->count('pivot.candidate_id') }} </strong></h1>
                    <h3>Jumlah Suara Masuk</h3>
                </div>
            </div>
        </div>
      </div>
     
    </div>
  </div>
  @endfor
</div>

@endsection
