@section('judul')
{{-- TEMPAT MEMBUAT JUDUL HALAMAN --}}
Data Hak Pilih (Siswa)
@endsection

@extends('template.template')

@push('script')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN JAVASCRIPT LIBRARY/CUSTOM --}}
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN CSS LIBRARY/CUSTOM --}}
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush 

@section('content')
{{-- BUAT KONTEN ANDA DIAREA SINI --}}

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Siswa</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
   <table id="example1" class="table table-striped">
        <thead>
            <tr>
            <th width="1%">No</th>
            <th>NIS</th>
            <th>Nama</th>
            <th>Tanggal Lahir</th>
            <th>Status Vote</th>
            @auth
                <th scope="col">Actions</th>
            @endauth
            </tr>
        </thead>
        <tbody>
            @forelse ($siswa as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->nis}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->tgl_lahir}}</td>
                    <td>
                        @if($value->status_vote =='0')   
                            <p class="text-danger"><b>Belum</b></p>
                        @else
                            <p class="text-primary"><b>Sudah</b></p>
                        @endif
                    </td>
                    @auth
                    <td display: inline>
                        <form action="/siswa/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/siswa/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/siswa/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                    @endauth

                </tr>
            @empty
                <tr>
                    <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
  </div>
</div>

@endsection
