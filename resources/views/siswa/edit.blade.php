@section('judul')
Edit Data Hak Pilih (Siswa)
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div  class="col-6">
        <form action="/siswa/{{$siswa->id}}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="nis">NIS </label>
                <input type="text" class="form-control" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <=57" name="nis" id="nis" placeholder="Masukkan NIS" value="{{$siswa->nis}}">
                @error('nis')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Nama </label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Lengkap" value="{{$siswa->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tgl_lahir">Tanggal Lahir</label>
                <input type="date" class="form-control"  name="tgl_lahir" id="tgl_lahir" value="{{$siswa->tgl_lahir}}">
                @error('tgl_lahir')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="class_id">Kelas</label>
                <select name="class_id" id="class_id" class="form-control"  value="{{$siswa->class_id}}">
                    @foreach ($class as $item)
                        <option value="{{$item->id}}"> {{$item->nama_kelas}}</option>
                    @endforeach
                </select>
                @error('class_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
</div>
@endsection
