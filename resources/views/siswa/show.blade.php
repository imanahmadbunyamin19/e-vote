@section('judul')
Detail Hak Pilih
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')

<table class="table">
    <tbody>
      <tr>
        <th scope="row">NIS</th>
        <td>{{$siswa->nis}}</td>
      </tr>
      <tr>
        <th scope="row">Nama Lengkap</th>
        <td>{{$siswa->nama}}</td>
      </tr>
      <tr>
        <th scope="row">Tanggal Lahir</th>
        <td>{{$siswa->tgl_lahir}}</td>
      </tr>
      <tr>
        <th scope="row">Status Vote</th>
        <td>
            @if($siswa->status_vote =='0')   
                <p class="text-danger"><b>Belum</b></p>
            @else
                <p class="text-primary"><b>Sudah</b></p>
            @endif
        </td>
      </tr>
      <tr>
        <th scope="row">Kelas</th>
        <td>{{$class->nama_kelas}}</td>
      </tr>
      <tr>
    </tbody>
  </table>

@endsection