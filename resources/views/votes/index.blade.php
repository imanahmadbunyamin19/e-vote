@section('judul')
Pemilihan Ketua Osis
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')

<div>
    
    <div class="row">
        
        @for ($i = 0; $i < $kandidat->count() ; $i++)
        <div class="col-4">
            <div class="col-md-12">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary ">
                    <h2 class="widget-user-username" style="background-color:black">
                        <strong> CALON NOMOR {{ $i+1 }} </strong>
                    </h2>
                    <h3 class="widget-user-username mt-1" style="color:black">
                        <strong> {{ $kandidat[$i]->nama_ketua }}  </strong>
                    </h3>

                    <h3 class="widget-user-username" style="color:black">
                        <strong> {{ $kandidat[$i]->nama_wakil }} </strong>
                    </h3>
                </div>

                <div >
                <img class="img-fluid img-thumbnail mt-2" src="{{ asset('fotoKandidat/' . $kandidat[$i]->foto) }}" alt="Foto Calon">
                </div>

                <div>
                    <div class="row">
                        <div class="col-12 text-center mt-2">
                            @method('POST')
                            <div class="col-12 text-center">
                                <form action="/kandidat/{{$kandidat[$i]->id}}" method="GET">
                                    @csrf
                                    <button type="submit" class="btn btn-primary btn-block">DETAILS</button>
                                </form>
                            </div>
                            <div class="col-12 text-center mt-2">
                                <form action="/vote" method="POST">
                                    @csrf
                                    <input type="hidden" name="student_id" id="student_id" value="{{ $user->siswa->id }}">
                                    <input type="hidden" name="candidate_id" id="candidate_id" value="{{$kandidat[$i]->id}}">
                                    
                                    <button type="submit" class="btn btn-danger btn-block">VOTE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        @endfor
    </div>
    
</div>
@endsection