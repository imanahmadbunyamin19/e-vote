<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('/images/LogoKelompok10.png')}}" alt="Logo Kelompok 10" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>E-VOTE</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @auth
            <a href="#" class="d-block"> <strong> {{ Auth::user()->name }} </strong> </a>
          @endauth

          @guest
          <a href="#" class="d-block">Anda Belum Login</a>
          @endguest
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>


          @auth
          @if (Auth::user()->role === 'admin' )
              
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa fa-cubes"></i>
                <p>
                  Data Master
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/admin" class="nav-link">
                    <i class="nav-icon fas fa fa-user-md"></i>
                    <p>Data Admin</p>
                  </a>
                </li>
              </ul>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/kelas" class="nav-link">
                    <i class="nav-icon fas fa fa-building"></i>
                    <p>Data Kelas</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="/kandidat" class="nav-link">
                <i class="nav-icon fas fa fa-male"></i>
                <p>
                  Data Kandidat
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/siswa" class="nav-link">
                <i class="nav-icon fas fa fa-users"></i>
                <p>
                  Data Hak Pilih (Siswa)
                </p>
              </a>
            </li>
          @else
              <li class="nav-item">
                <a href="/vote" class="nav-link">
                  <i class="nav-icon fas fa fa-check-square"></i>
                  <p>
                    VOTE DISINI !
                  </p>
                </a>
              </li>
          @endif
          @endauth

          @guest
          <li class="nav-item">
            <a href="/login" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                Login
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
              @csrf 
            </form>
          </li>
          @endguest

          @auth
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa fa-times-circle"></i>
                Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
              @csrf 
            </form>
          </li>
          @endauth

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>