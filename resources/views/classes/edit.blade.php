@section('judul')
Edit Kelas
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div>
        <form action="/kelas/{{$class->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama_kelas">Kelas</label>
                <input type="text"  value="{{$class->nama_kelas}}" id="nama_kelas" name="nama_kelas" class="form-control" >
                @error('nama_kelas')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
</div>
@endsection
