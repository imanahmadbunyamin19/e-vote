@section('judul')
Input Kelas
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div>
        <form action="/kelas" method="POST">
            @csrf
            <div class="form-group">
                <input type="hidden" class="form-control" name="status" id="status" value="0">
            </div>

            <div class="form-group">
                <label for="class_id">Kelas</label>
                <input type="class_id" id="class_id" name=nama_kelas class="form-control" >
                @error('class_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
</div>
@endsection
