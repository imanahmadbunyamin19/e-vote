@section('judul')
{{-- TEMPAT MEMBUAT JUDUL HALAMAN --}}
Edit Kandidat
@endsection

@extends('template.template')

@push('script')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN JAVASCRIPT LIBRARY/CUSTOM --}}
<script src="{{asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('#visi').summernote()
    $('#misi').summernote()

  })
</script>
@endpush

@push('style')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN CSS LIBRARY/CUSTOM --}}
<link rel="stylesheet" href="{{asset('adminlte/plugins/summernote/summernote-bs4.min.css')}}">
@endpush

@section('content')
{{-- BUAT KONTEN ANDA DIAREA SINI --}}

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Form Edit Kandidat</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <form action="/kandidat/{{$kandidat->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="card-body">
      <div class="form-group">
        <label>Nama Calon Ketua</label>
        <input type="text" class="form-control" id="ketua" name="ketua" placeholder="Masukkan nama calon ketua" value="{{$kandidat->nama_ketua}}">
        @error('ketua')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label>Nama Calon Wakil</label>
        <input type="text" class="form-control" id="wakil" name="wakil" placeholder="Masukkan nama calon wakil" value="{{$kandidat->nama_wakil}}">
        @error('wakil')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label>Visi</label>
        <textarea id="visi" class="form-control" name="visi">{{$kandidat->visi}}</textarea>
        @error('visi')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label>Misi</label>
        <textarea id="misi" class="form-control" name="misi">{{$kandidat->misi}}</textarea>
        @error('misi')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label>Thumbnail </label>
        <input type="file" class="form-control" id="foto" name="foto">
        (<small class="text-muted">Kosongkan apabila tidak ingin menganti thumbnail</small>)
        @error('foto')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="form-group">
        <label>Thumbnail Sekarang</label><br>
        <img src="{{asset('fotoKandidat/')}}/{{$kandidat->foto}}" width="50%" class="mt-1" >
      </div>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
  </form>
</div>

@endsection