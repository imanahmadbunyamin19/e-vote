@section('judul')
{{-- TEMPAT MEMBUAT JUDUL HALAMAN --}}
Detail Data 
@endsection

@extends('template.template')

@push('script')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN JAVASCRIPT LIBRARY/CUSTOM --}}
@endpush

@push('style')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN CSS LIBRARY/CUSTOM --}}

@endpush

@section('content')
{{-- BUAT KONTEN ANDA DIAREA SINI --}}
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Detail data</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <a href="/kandidat" class="btn btn-danger mt-2 mb-2">Kembali</a>
    <div class="row">
      @foreach($data as $key=>$value)
      <div class="col-md-5">
        <img src="{{asset('fotoKandidat/')}}/{{ $value['foto'] }}" width="100%" class="mt-1" >
      </div>
      <div class="col-md-7">
        <table class="table table-bordered">
          <tr>
            <td width="30%">Nama Ketua</td>
            <td>{{ $value['nama_ketua'] }}</td>
          </tr>
          <tr>
            <td>Nama Wakil</td>
            <td>{{ $value['nama_wakil'] }}</td>
          </tr>
          <tr>
            <td>Jumlah Suara</td>
            <td>{{ $value['jumlah_suara'] }}</td>
          </tr>
          <tr>
            <td>Visi</td>
            <td>{!! $value['visi'] !!}</td>
          </tr>
          <tr>
            <td>Misi</td>
            <td>{!! $value['misi'] !!}</td>
          </tr>
        </table>
      </div>
      @endforeach
    </div>
  </div>
</div>
@endsection
