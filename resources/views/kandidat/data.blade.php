@section('judul')
{{-- TEMPAT MEMBUAT JUDUL HALAMAN --}}
Kandidat
@endsection

@extends('template.template')

@push('script')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN JAVASCRIPT LIBRARY/CUSTOM --}}
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
{{-- TEMPAT LINK UNTUK MENAMBAHKAN CSS LIBRARY/CUSTOM --}}
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush 

@section('content')
{{-- BUAT KONTEN ANDA DIAREA SINI --}}

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Kandidat</h3>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <a href="/kandidat/create" class="btn btn-primary mb-3">Tambah</a>
    <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th width="1%">No</th>
            <th>Nama Ketua</th>
            <th>Nama Wakil</th>
            <th>Suara Diperoleh</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
             @forelse ($data as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value['nama_ketua']}}</td>
                        <td>{{$value['nama_wakil']}}</td>
                        <td>{{$value['jumlah_suara']}}</td>
                        <td>
                          <form action="/kandidat/{{$value['id']}}" method="POST">
                            <a href="/kandidat/{{$value['id']}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/kandidat/{{$value['id']}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No data</td>
                    </tr>  
                @endforelse   
          </tbody>
        </table>
  </div>
</div>

@endsection
