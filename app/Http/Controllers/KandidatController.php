<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kandidat;
use Illuminate\Support\Facades\DB;

class KandidatController extends Controller
{
    public function index(){
        $data = [];
        $kandidat = Kandidat::all();
        foreach ($kandidat as $key) {
            $suara = DB::select('SELECT COUNT(candidate_id) AS jumlah_suara FROM votes WHERE candidate_id=:id', ['id' => $key['id']]);
            foreach($suara as $resultSuara){
                $result = array(
                    "id" => $key['id'],
                    "nama_ketua" => $key['nama_ketua'],
                    "nama_wakil" => $key['nama_wakil'],
                    "jumlah_suara" => $resultSuara -> jumlah_suara
                );
            }
            array_push($data, $result);
        }
        return view('kandidat.data', compact('data'));
    }

    public function create(){
        return view('kandidat.create');
    }

    public function store(Request $request){
        $request -> validate(
            [
                'ketua' => 'required',
                'wakil' => 'required',
                'visi' => 'required',
                'misi' => 'required',
                'foto' => 'required|image|mimes:jpeg,png,jpg'
            ],
            [
                'ketua.required' => 'Nama Ketua Harus diisi',
                'wakil.required' => 'Nama Wakil Harus diisi',
                'visi.required' => 'Visi Harus diisi',
                'misi.required' => 'Misi Harus diisi',
                'foto.required' => 'Thumbnail kandidat harus diupload!'
            ]
        );

        if ($files = $request->file('foto')) {
           $destinationPath = public_path('/fotoKandidat/');
           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           $input['foto'] = "$profileImage";       
       }

       DB::table('candidates')->insert(
        [
            'nama_ketua' => $request['ketua'],
            'nama_wakil' => $request['wakil'],
            'visi' => $request['visi'],
            'misi' => $request['misi'],
            'foto' => "$profileImage"
        ]
        );
       return redirect('/kandidat');
   }

    public function edit($id){
        $kandidat = Kandidat::findOrFail($id);
        return view('kandidat.edit', compact('kandidat'));
    }

    public function update(Request $request, $id){
        $request -> validate(
            [
                'ketua' => 'required',
                'wakil' => 'required',
                'visi' => 'required',
                'misi' => 'required'
            ],
            [
                'ketua.required' => 'Nama Ketua Harus diisi',
                'wakil.required' => 'Nama Wakil Harus diisi',
                'visi.required' => 'Visi Harus diisi',
                'misi.required' => 'Misi Harus diisi'
            ]
        );

        if ($files = $request->file('foto')) {
            // Untuk menghapus foto
            $kandidat = Kandidat::find($id);
            if ($kandidat->foto != "") {
                unlink(public_path("fotoKandidat/").$kandidat->foto);
            }

            $destinationPath = public_path('/fotoKandidat/');
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $query = DB::table('candidates')
                ->where('id', $id)
                ->update([
                    'nama_ketua' => $request['ketua'],
                    'nama_wakil' => $request['wakil'],
                    'visi' => $request['visi'],
                    'misi' => $request['misi'],
                    'foto' => "$profileImage"
            ]);   
       }else{
            $query = DB::table('candidates')
                ->where('id', $id)
                ->update([
                    'nama_ketua' => $request['ketua'],
                    'nama_wakil' => $request['wakil'],
                    'visi' => $request['visi'],
                    'misi' => $request['misi']
            ]);
       }
       return redirect('/kandidat');
    }

    public function show($id){
        $data = [];
        $kandidat = DB::select('SELECT * FROM candidates WHERE id=:id', ['id' => $id]);
        foreach ($kandidat as $key) {
            $suara = DB::select('SELECT COUNT(candidate_id) AS jumlah_suara FROM votes WHERE candidate_id=:id', ['id' => $id]);
            foreach($suara as $resultSuara){
                $result = array(
                    "nama_ketua" => $key->nama_ketua,
                    "nama_wakil" => $key->nama_wakil,
                    "visi" => $key->visi,
                    "misi" => $key->misi,
                    "foto" => $key->foto,
                    "jumlah_suara" => $resultSuara->jumlah_suara
                );
            }

            array_push($data, $result);
        }
        
        return view('kandidat.detail', compact('data'));
    }

    public function destroy($id){
        $kandidat = Kandidat::withCount($id);
        if ($kandidat->foto != "") {
                unlink(public_path("fotoKandidat/").$kandidat->foto);
        }

        $kandidat->delete();
        return redirect('/kandidat');
    }
} 
