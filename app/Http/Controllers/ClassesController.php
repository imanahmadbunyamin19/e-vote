<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;
use Illuminate\Support\Facades\DB;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $class = DB::table('classes')->get();

        return view('classes.index',compact('class'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $class = Kelas::all();
        return view('classes.create',compact('class'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate(
            [
                'nama_kelas' => 'required'
            ],
            [
                'nama_kelas.required' => 'Masukan Kelas'
            ]
            );

        DB::table('classes')->insert(
            [
    
                'nama_kelas' => $request['nama_kelas']
            ]
        );
        return redirect('/kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = DB::table('classes')->where('id',$id)->first();

        return view('classes.show',compact('class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = DB::table('classes')->where('id',$id)->first();
        return view('classes.edit', compact('class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate(
            [
                'nama_kelas' => 'required'
            ],
            [
                'nama_kelas.required' => 'Masukan Kelas'
            ]
        );

        DB::table('classes')->where('id',$id)->update(
            [
                'nama_kelas' => $request['nama_kelas']
            ]
        );

        $siswa = Kelas::find($id);

        $siswa -> nama_kelas = $request-> nama_kelas;   

        $siswa -> save();


        return redirect('/kelas');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('classes')->where('id', '=', $id)->delete();

        return redirect('/kelas');
    }
}
