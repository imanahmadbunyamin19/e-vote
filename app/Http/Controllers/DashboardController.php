<?php

namespace App\Http\Controllers;

use App\Kelas;
use Illuminate\Http\Request;
use App\Siswa;
use Illuminate\Support\Facades\DB;
class DashboardController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        $jmlsiswa = Siswa::all()->count();
        $jmlkelas = Kelas::all()->count();
        $kandidat = DB::table('candidates')->get();
        $jmlkandidat = DB::table('candidates')->count();
        $jumlahvotemasuk = DB::table('votes')->count();       
        $jumlahvotekandidat = DB::table('votes')->get();
        
        return view('dashboard', compact('jmlsiswa','jmlkelas', 'kandidat', 'jmlkandidat', 'jumlahvotemasuk','jumlahvotekandidat'));
    }
}
