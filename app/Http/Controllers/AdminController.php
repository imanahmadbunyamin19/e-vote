<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Kelas;
use App\User;

class AdminController extends Controller

{
    public function index()
    {
        $admin = DB::table('users')->where('role','admin')->get();
        return view('admin.index',compact('admin'));
    }

    public function create()
    {
        $class = Kelas::all();
        return view('admin.create',compact('class'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => ['required','unique:users,email', 'string'],
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ],
        [
            'email.required' => 'Email Harus diisi',
            'email.unique' => 'Email sudah terdaftar',
            'password.required' => 'Password Tidak Boleh kosong',
            'password.confirmed' => 'Password tidak sama',
        ]);

        $query = DB::table('users')->insert([
            "email" => $request["email"],
            "name" => $request["name"],
            "password" => Hash::make($request['password']),
            "role" => 'admin'
        ]);

        return redirect('/admin');
    }

    public function edit($id)
    {
        $admin = DB::table('users')->where('id',$id)->first();
        return view('admin.edit', compact('admin'));
    }
   
    public function update(Request $request, $id)
    {
        $request -> validate(
            [
                'email' => 'required',
                'name' => 'required',
            ],
            [
                'email.required' => 'email Harus diisi',
                'name.required' => 'Nama Harus diisi',
            ]
        );

        $admin = User::find($id);

        $admin -> email = $request-> email;
        $admin -> name = $request-> name;

        if($request->password=='null'){
        //    do nothing
        }else{
            $admin -> password = Hash::make($request['password']);
        };

        $admin -> save();

        return redirect('/admin');
    }


    public function destroy($id)
    {
        DB::table('users')->where('id', '=', $id)->delete();

        return redirect('/admin');
    }
}
