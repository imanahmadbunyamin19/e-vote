<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Siswa;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    public function index()
    {
        $kandidat = DB::table('candidates')->get();
        $jmlkandidat = DB::table('candidates')->count();       
        $vote = DB::table('votes')->get();
        $siswa = Siswa::all();
        $user = Auth::user();
        return view('votes.index',compact('kandidat','jmlkandidat','vote','siswa','user'));
    }
 
    public function store(Request $request)
    {
        
        $current_date_time = Carbon::now()->toDateTimeString();
        
        DB::table('votes')->insert( 
            [
                'waktu_pilih' => $current_date_time,
                'candidate_id' => $request['candidate_id'],
                'student_id' => $request['student_id'],
            ]
        );

        // update status vote
        $siswa = Siswa::find($request['student_id']);
        $siswa -> status_vote = 1;  
        $siswa -> save();
        
        return redirect('/');
    }
    
}
