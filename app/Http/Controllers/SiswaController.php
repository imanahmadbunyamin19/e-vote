<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Kelas;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Null_;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = Siswa::all();
        return view('siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $class = Kelas::all();
        $siswa = Siswa::all();
        return view('siswa.create', compact('siswa','class'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate(
            [
                'nis' => 'required',
                'nama' => 'required',
                'nama' => 'required',
                'tgl_lahir' => 'required',
                'nama' => 'required',
                'class_id' => 'required'
            ],
            [
                'nis.required' => 'NIS Harus diisi',
                'nama.required' => 'Nama Harus diisi',
                'tgl_lahir.required' => 'Tanggal Lahir Harus diisi',
                'class_id.required' => 'Kelas Belum dipilih'
            ]
            );

        DB::table('students')->insert(
            [
                'nis' => $request['nis'],
                'nama' => $request['nama'],
                'tgl_lahir' => $request['tgl_lahir'],
                'status_vote' => $request['status'],
                'class_id' => $request['class_id']
            ]
        );
        return redirect('/siswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siswa = Siswa::findOrFail($id);
        $class = Kelas::findOrFail($siswa->class_id);
        return view('siswa.show', compact('siswa','class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = Kelas::all();
        $siswa = Siswa::findOrFail($id);
        return view('siswa.edit', compact('siswa','class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate(
            [
                'nis' => 'required',
                'nama' => 'required',
                'tgl_lahir' => 'required',
                'class_id' => 'required'
            ],
            [
                'nis.required' => 'NIS Harus diisi',
                'nama.required' => 'Nama Harus diisi',
                'tgl_lahir.required' => 'Tanggal Lahir Harus diisi',
                'class_id.required' => 'Kelas Belum dipilih'
            ]
        );
        $siswa = Siswa::find($id);

        $siswa -> nis = $request-> nis;
        $siswa -> nama = $request-> nama;
        $siswa -> tgl_lahir = $request-> tgl_lahir;
        $siswa -> class_id = $request-> class_id;      

        $siswa -> save();

        return redirect('/siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::find($id);
        $siswa->delete();

        return redirect('/siswa');
    }
}
