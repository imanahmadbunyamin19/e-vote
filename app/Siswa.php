<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'students';
    protected $fillable = ['nis','nama','tgl_lahir','status_vote','class_id','user_id'];


    public function user()
    {
       return $this->belongsTo('App\User');
    }
}

