<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kandidat extends Model
{
    protected $table = "candidates";
    protected $fillable = ['nama_ketua','nama_wakil','visi','misi','foto'];

    public function vote()
    {
        return $this->hasOne('App\Vote');
    }
}
