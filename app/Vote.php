<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $table = 'votes';
    protected $fillable = ['waktu_pilih','candidate_id','student_id'];

    public function kandidat()
    {
        return $this->belongsTo('App\Kandidat');
    }
}
