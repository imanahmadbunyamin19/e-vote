<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Kandidat routes
Route::get('/kandidat', 'KandidatController@index')->middleware('auth');
Route::get('/kandidat/create', 'KandidatController@create')->middleware('auth');
Route::post('/kandidat', 'KandidatController@store')->middleware('auth');
Route::get('/kandidat/{kandidat_id}/edit', 'KandidatController@edit')->middleware('auth');
Route::put('/kandidat/{kandidat_id}', 'KandidatController@update')->middleware('auth');
Route::delete('/kandidat/{kandidat_id}', 'KandidatController@destroy')->middleware('auth');
Route::get('/kandidat/{kandidat_id}', 'KandidatController@show');

//Siswa routes
Route::get('/siswa', 'SiswaController@index')->middleware('auth');
Route::get('/siswa/create', 'SiswaController@create')->middleware('auth');
Route::post('/siswa', 'SiswaController@store')->middleware('auth');
Route::get('/siswa/{siswa_id}', 'SiswaController@show')->middleware('auth');
Route::get('/siswa/{siswa_id}/edit', 'SiswaController@edit')->middleware('auth');
Route::put('/siswa/{siswa_id}', 'SiswaController@update')->middleware('auth');
Route::delete('/siswa/{siswa_id}', 'SiswaController@destroy')->middleware('auth');

//clases
Route::get('/kelas/create', 'ClassesController@create')->middleware('auth');
Route::post('/kelas', 'ClassesController@store')->middleware('auth');
/*----------*/
Route::get('/kelas', 'ClassesController@index')->middleware('auth');
Route::get('/kelas/{kelas_id}', 'ClassesController@show')->middleware('auth');
/*----------*/
Route::get('/kelas/{kelas_id}/edit', 'ClassesController@edit')->middleware('auth');
Route::put('/kelas/{kelas_id}', 'ClassesController@update')->middleware('auth');
/*----------*/
Route::delete('/kelas/{kelas_id}', 'ClassesController@destroy')->middleware('auth');
//authentication routes

//votes
Route::get('/vote', 'VoteController@index')->middleware('auth');
Route::post('/vote', 'VoteController@store')->middleware('auth');

Auth::routes();

//admin routes
Route::get('/admin', 'AdminController@index');
Route::get('/admin/create', 'AdminController@create');
Route::post('/admin', 'AdminController@store');
Route::get('/admin/{user_id}', 'AdminController@show');
Route::get('/admin/{user_id}/edit', 'AdminController@edit');
Route::put('/admin/{user_id}', 'AdminController@update');
Route::delete('/admin/{user_id}', 'AdminController@destroy');


//route ke dashboard 
Route::get('/', 'DashboardController@index');
